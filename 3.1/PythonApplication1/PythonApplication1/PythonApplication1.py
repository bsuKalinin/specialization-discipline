import random

def task1a():
    first = (1, [2,3])
    first[1].append(4)
    print(first)

def task1b():
    a = (1,[2,3])
    c = a + ([4,5,6],)
    print(c)

def task1c():
    a = (1,[2,3])
    b = a + ("abc",)
    print(b)

def task1d():
    a = (1,[2,3])
    c = (a[0],a[1] + a[1])
    print(c)

def task2a():
    print(len(set(input().split()) & set(input().split())))

def task2b():
    string = input("Enter numbers: ").split()
    d = dict()
    for numb in string:
        d[numb] = "Yes" if numb in d else "No"
    print(d)

def task2c():
    n = int(input("Anya's bricks count: "))
    m = int(input("Borya's bricks count: "))
    boryaSet = set()
    anyaSet = set()
    i = 0
    while i < max(n,m):
        if (i < n):
            number1 = random.randint(0, 108)
            boryaSet.add(number1)
        if (i < m):
            number2 = random.randint(0, 108)
            anyaSet.add(number2)
        i += 1
    print(anyaSet)
    print(boryaSet)

    print(f'count: {len((anyaSet & boryaSet))}; set: {sorted((anyaSet & boryaSet))}')
    print(f'count: {len((anyaSet.difference(boryaSet)))}; set: {sorted((anyaSet.difference(boryaSet)))}')
    print(f'count: {len((boryaSet.difference(anyaSet)))}; set: {sorted((boryaSet.difference(anyaSet)))}')

def task2d():
    n = int(input())
    allNums = set(range(1, 2 * n))
    possibleNums = allNums
    while True:
        guess = input()
        if guess == 'IDONTKNOW':
            print(' '.join([str(x) for x in sorted(possibleNums)]))
            break
        guess = {int(x) for x in guess.split()}
        answer = input()
        if answer == 'YES':
            possibleNums &= guess
        else:
            possibleNums &= allNums - guess

def task3a():
    dictionary = {}
    str = input('enter string: ').split(' ')
    for word in str:
        dictionary[word] = dictionary.get(word, 0) + 1
    print(dictionary)

def task3b():
    n = int(input())
    keyValue = dict(input().split() for j in range(n))
    k = input()
    for key, value in keyValue.items():
        if k == value:
            print(key)
        if k == key:
            print(value)

def task3c():
    dictionary = {}
    for i in range(int(input())):
        for word in input().split():
            dictionary[word] = dictionary.get(word, 0) + 1
    print(min([key for key, value in dictionary.items() if value == max(dictionary.values())]))

def task3d():
    d = {}
    for i in range(int(input())):
        for word in input().split():
            d[word] = d.get(word, 0) + 1  
    for word in sorted(d.items(), key=lambda x: (-x[1], x[0])):
        print(word[0])


#task1a()
#task1b()
#task1c()
#task1d()
#task2a()
#task2b()
#task2c()
#task2d()
#task3a()
#task3b()
#task3c()
task3d()