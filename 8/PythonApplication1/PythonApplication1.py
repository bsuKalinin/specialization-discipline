import numpy
from PIL import Image
from keras.models import model_from_json


def cifar():
    json_filename = "cifar_model.json"
    with open(json_filename, "r") as json_file:
        loaded_model_json = json_file.read()

    model = model_from_json(loaded_model_json)

    h5_filename = "cifar_model.h5"
    model.load_weights(h5_filename)

    model.compile(loss="categorical_crossentropy", optimizer="SGD",
                  metrics=["accuracy"])

    pic_name = "motorcycle.jpg"
    img = Image.open(pic_name)

    arr = numpy.array(img)

    new_arr = arr.reshape(1, 32, 32, 3).astype('float32')

    new_arr /= 255

    answ = ((model.predict(new_arr)).tolist())[0]
    answs = ['рыба', 'вертолёт', 'самолет', 'велосипед', 'птица', 'кот', 'дом', 'собака', 'мотоцикл', 'грузовик']
    print('result: ', answs[numpy.argmax(answ)])


def mnist():
    json_filename = "mnist_model.json"
    with open(json_filename, "r") as json_file:
        loaded_model_json = json_file.read()

    model = model_from_json(loaded_model_json)

    h5_filename = "mnist_model.h5"
    model.load_weights(h5_filename)

    model.compile(loss="categorical_crossentropy", optimizer="SGD",
                  metrics=["accuracy"])

    pic_name = "1.png"
    img = Image.open(pic_name)

    arr = numpy.array(img)

    new_arr = numpy.array([
        [255 - pixel[0] for row in arr for pixel in row]
    ], 'float32')

    new_arr = new_arr.reshape(1, 28, 28, 1).astype('float32')

    new_arr = new_arr / 255

    print('result: ', numpy.argmax(model.predict(new_arr)))


cifar()
mnist()
