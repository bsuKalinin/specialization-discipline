from keras.applications.resnet50 import ResNet50
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input, decode_predictions
import numpy as np

model = ResNet50(weights='imagenet')

img_path = 'motorcycle.png'
img = image.load_img(img_path, target_size=(360, 360))
imgToArr = image.img_to_array(img)
expDims = np.expand_dims(imgToArr, axis=0)
preprocess = preprocess_input(expDims)

preds = model.predict(preprocess)
print('result:', decode_predictions(preds, top=1)[0])