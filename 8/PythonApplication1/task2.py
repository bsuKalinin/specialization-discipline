from keras.applications.vgg16 import VGG16, decode_predictions
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
import numpy as np

model = VGG16(weights='imagenet', include_top=True)

img_path = 'motorcycle.png'
img = image.load_img(img_path, target_size=(360, 360))
imgToArray = image.img_to_array(img)
expandDims = np.expand_dims(imgToArray, axis=0)
preprocessInput = preprocess_input(expandDims)

features = model.predict(preprocessInput)

print('result:', decode_predictions(features, top=1)[0])