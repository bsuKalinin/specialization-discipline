import math
import random
import mymodule

def task1ab():
    f = open('Surnames.txt', 'r')
    counter = 1
    for surname in  f.readlines():
        print(str(counter) + " " + surname)
        counter+=1

def task1c():
    f = open('SurnamesNames.txt', 'r')
    counter = 1
    for surname in  f.readlines():
        print(str(counter) + " " + surname)
        counter+=1

def task1d():
    f = open('SurnamesNames.txt', 'r')
    for surname in  f.readlines():
        print(surname.split(' ')[0] + " " + surname.split(' ')[1][0])

def task2a():
    print(math.log(15,2))

def task2b():
    random.seed(2)
    print(random.random())
    random.seed(2)
    print(random.random())
    random.seed(2)
    print(random.random())
    random.seed(2)
    print(random.random())

def task2c():
    print("не понимаю")

def task2d():
    mymodule.hello()

#task1ab()
#task1c()
#task1d()
#task2a()
#task2b()
#task2c()
task2d()