import math
import random

def distance(x1,y1,x2,y2):
    return math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))

def power(a,n):
    s = 1
    if n == 0: 
        return 1

    for i in range(abs(n)):
        s *= a
    if n < 0: 
         s = 1 / s
    return s

def capitalize(word):
    return chr(ord(word[0]) - 32) + word[1:]

def max(*l):
    max = l[0][0]
    for number in l[0]:
        if max < number:
            max = number
    return max

def task1ae():
    try:
        x1 = float(input())
        y1 = float(input())
        x2 = float(input())
        y2 = float(input())
        print(distance(x1,y1,x2,y2))
    except ValueError:
        print("Value Error")
    except OverflowError:
        print("Error")

def task1b():
    a = int(input())
    n = int(input())
    print(f'{power(a,n)} {pow(a,n)}')

def task1c():
    phrase = input("Enter phrase: ").split(' ')
    for word in phrase:
        print(capitalize(word))

def task1d():
    list = []
    for i in range(10):
        list.append(random.randint(1, 100))
    print(list)
    print(max(list))


#task1ae()
#task1b()
#task1c()
#task1d()